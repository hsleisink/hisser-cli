Hisser Command Line Client
==========================

This Hisser command line client has been written in PHP and is for testing and demo purposes only.

Installation
------------

First, make sure you have a Hisser server up and running and you have an account on that server.

Copy the files to a suitable location and run initdb to initialize the local SQLite3 database. This will create a storage.db file.

Usage
-----

Run the hisser PHP script. Start by adding other users on your contact list by inviting them. A hisser address consists of a username followed by a @ and the hostname of the Hisser server.
