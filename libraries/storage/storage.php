<?php
	/* libraries/storage/storage.php
	 *
	 * Copyright (C) by Hugo Leisink <hugo@leisink.net>
	 */

	class storage extends sqlite3_connection {
		private $dh_prime = null;
		private $dh_generator = null;
		
		public function __construct($storage_db, $prime, $generator) {
			$this->dh_prime = $prime;
			$this->dh_generator = $generator;

			parent::__construct($storage_db, SQLITE3_OPEN_READWRITE | SQLITE3_OPEN_CREATE);
		}

		/* Get setting from database
		 *
		 * INPUT:  string key
		 * OUTPUT: string setting for key
		 * ERROR:  -
		 */
		public function get_setting($key) {
			static $cache = array();

			if (isset($cache[$key]) == false) {
				if (($result = $this->entry("settings", $key, "key")) == false) {
					return null;
				}

				return $cache[$key] = $result["value"];
			}

			return $cache[$key];
		}

		/* Store setting in database
		 *
		 * INPUT:  string key, mixed value
		 * OUTPUT: true
		 * ERROR:  false
		 */
		public function set_setting($key, $value) {
			$query = "select count(*) as count from settings where key=%s";
			if (($result = $this->execute($query, $key)) === false) {
				return false;
			}

			if ($result[0]["count"] == 0) {
				return $this->insert("settings", array("key" => $key, "value" => $value)) !== false;
			} else {
				$query = "update settings set value=%s where key=%s";
				return $this->query($query, $value, $key) !== false;
			}
		}

		/* Create contact record
		 *
		 * INPUT:  string address, string alias, string DH value
		 * OUTPUT: true
		 * ERROR:  false
		 */
		public function create_contact($address, $my_alias, $value_a) {
			if ($this->query("begin") === false) {
				return false;
			}

			$data = array(
				"id"         => null,
				"name"       => null,
				"address"    => $address,
				"alias"      => null,
				"my_alias"   => $my_alias,
				"validated"  => 0);

			if ($this->insert("contacts", $data) === false) {
				$this->query("rollback");
				return false;
			}
			$contact_id = $this->last_insert_id;

			$data = array(
				"id"         => null,
				"contact_id" => $contact_id,
				"serial_a"   => 0,
				"serial_b"   => null,
				"value_a"    => $value_a,
				"value_b"    => null,
				"crypto_key" => null,
				"confirmed"  => 0);

			if ($this->insert("dh_values", $data) === false) {
				$this->query("rollback");
				return false;
			}

			return $this->query("commit") !== false;
		}

		/* Add information to an incomplete contact record
		 *
		 * INPUT:  integer contact id, string name, string alias, string public key, string DH value
		 * OUTPUT: true
		 * ERROR:  false
		 */
		public function complete_contact($contact_id, $name, $alias, $public_key, $value_b) {
			if ($this->query("begin") === false) {
				return false;
			}

			$data = array(
				"name"       => $name,
				"alias"      => $alias,
				"public_key" => $public_key);

			if ($this->update("contacts", $contact_id, $data) === false) {
				$this->query("rollback");
				return false;
			}

			$query = "update dh_values set serial_b=%d, value_b=%s, confirmed=%d ".
			         "where contact_id=%d and serial_a=%d";
			if ($this->query($query, 0, $value_b, 1, $contact_id, 0) === false) {
				$this->query("rollback");
				return false;
			}

			return $this->query("commit") !== false;
		}

		/* Validate contact
		 *
		 * INPUT:  integer contact id
		 * OUTPUT: true
		 * ERROR:  false
		 */
		public function validate_contact($contact_id) {
			$query = "update contacts set validated=%d where id=%d";

			return $this->query($query, 1, $contact_id) !== false;
		}

		/* Get contact by address
		 *
		 * INPUT:  string Hisser address
		 * OUTPUT: array contact
		 * ERROR:  false
		 */
		public function get_contact_by_address($address) {
			$query = "select * from contacts where address=%s";
			if (($result = $this->execute($query, $address)) == false) {
				return false;
			}

			return $result[0];
		}

		/* Get contact by my alias
		 *
		 * INPUT:  string alias
		 * OUTPUT: array contact
		 * ERROR:  false
		 */
		public function get_contact_by_my_alias($my_alias) {
			$query = "select * from contacts where my_alias=%s";
			if (($result = $this->execute($query, $my_alias)) == false) {
				return false;
			}

			return $result[0];
		}

		/* Get all contacts
		 *
		 * INPUT:  -
		 * OUTPUT: array contact arrays
		 * ERROR:  false
		 */
		public function get_contacts() {
			$query = "select * from contacts order by name";

			return $this->execute($query);
		}

		/* Remove contact from contact list
		 *
		 * INPUT:  string Hisser address
		 * OUTPUT: true
		 * ERROR:  false
		 */
		public function remove_contact($address) {
			if ($this->get_contact_by_address($address) == false) {
				return false;
			}

			$query = "delete from contacts where address=%s";

			return $this->query($query, $address) !== false;
		}

		/* Store message in database
		 *
		 * INPUT:  integer contact id, string message
		 * OUTPUT: true
		 * ERROR:  false
		 */
		public function save_message($contact_id, $message) {
			$data = array(
				"id"         => null,
				"contact_id" => $contact_id,
				"content"    => $message);

			return $this->insert("messages", $data) !== false;
		}

		/* Get messages from database
		 *
		 * INPUT:  -
		 * OUTPUT: array messages
		 * ERROR:  false
		 */
		public function get_message_index() {
			$query = "select m.id, m.content, c.name, c.address ".
			         "from messages m, contacts c where m.contact_id=c.id";

			return $this->execute($query);
		}

		/* Get single message from database
		 *
		 * INPUT:  integer message id
		 * OUTPUT: array message
		 * ERROR:  false
		 */
		public function get_message($message_id) {
			$query = "select * from messages m, contacts c where m.id=%d and m.contact_id=c.id";

			if (($result = $this->execute($query, $message_id)) == false) {
				return false;
			}

			return $result[0];
		}

		/* Delete message from database
		 *
		 * INPUT:  integer message id
		 * OUTPUT: true
		 * ERROR:  false
		 */
		public function delete_message($message_id) {
			if ($this->get_message($message_id) == false) {
				return false;
			}

			return $this->delete("messages", $message_id) !== false;
		}

		/* Get AES key and DH values for latest confirmed DH value
		 *
		 * INPUT:  integer contact id
		 * OUTPUT: array crypto keys
		 * ERROR:  false
		 */
		public function get_crypto_values($contact_id) {
			$query = "select * from dh_values where contact_id=%d and confirmed=%d order by id desc limit 1";
			if (($result = $this->execute($query, $contact_id, 1)) == false) {
				return false;
			}
			$values = $result[0];

			if ($values["crypto_key"] == null) {
				#printf("CRYPTO: Diffie-Hellman: calculating secret key for %d and %d...\n", $values["value_a"], $values["value_b"]);
				$dh = new DH($this->dh_prime, $this->dh_generator, $values["value_a"]);
				$values["crypto_key"] = $dh->calculate_key($values["value_b"]);

				$this->update("dh_values", $values["id"], array("crypto_key" => $values["crypto_key"]));
			}

			$query = "select count(*) as count from dh_values where contact_id=%d and serial_a>%d and confirmed=%d";
			if (($result = $this->execute($query, $contact_id, $values["serial_a"], 0)) == false) {
				return false;
			}

			if ($result[0]["count"] == 0) {
				/* Generate new A
				 */
				$dh = new DH($this->dh_prime, $this->dh_generator);
				$new_value_a = $dh->private_value;
				$new_serial_a = $values["serial_a"] + 1;

				$query = "select serial_b, value_b from dh_values where contact_id=%d order by serial_b desc limit 1";
				if (($result = $this->execute($query, $contact_id)) == false) {
					return false;
				}
				$last_value_b = $result[0]["value_b"];
				$last_serial_b = $result[0]["serial_b"];

				/* Save record
				 */
				$data = array(
					"id"         => null,
					"contact_id" => $contact_id,
					"serial_a"   => $new_serial_a,
					"serial_b"   => $last_serial_b,
					"value_a"    => $new_value_a,
					"value_b"    => $last_value_b,
					"crypto_key" => null,
					"confirmed"  => 0);

				if ($this->insert("dh_values", $data) === false) {
					return false;
				}
			}

			#printf("CRYPTO: %d %d -> %s\n", $values["serial_a"], $values["serial_b"], $values["crypto_key"]);

			return $values;
		}

		/* Get my latest DH values
		 *
		 * INPUT:  integer contact id
		 * OUTPUT: array DH values
		 * ERROR:  false
		 */
		public function get_my_latest_dh_value($contact_id) {
			$query = "select serial_a, value_a from dh_values where contact_id=%d order by serial_a desc limit 1";
			if (($result = $this->execute($query, $contact_id)) == false) {
				return false;
			}

			$dh = new DH($this->dh_prime, $this->dh_generator, $result[0]["value_a"]);

			return array(
				"dh_serial" => $result[0]["serial_a"],
				"dh_value"  => $dh->public_value);
		}

		/* Get AES key for decryption by DH serial numbers
		 *
		 * INPUT:  integer contact id, integer DH serial A, integer DH serial B
		 * OUTPUT: string AES key
		 * ERROR:  false
		 */
		public function get_key_for_decryption($contact_id, $serial_b, $serial_a) {
			$query = "select * from dh_values where contact_id=%d and serial_a=%d and serial_b=%d";
			if (($result = $this->execute($query, $contact_id, $serial_a, $serial_b)) == false) {
				/* Looking for A
				 */
				$query = "select * from dh_values where contact_id=%d and serial_a=%d";
				if (($result = $this->execute($query, $contact_id, $serial_a)) == false) {
					return false;
				}
				$value_a = $result[0]["value_a"];

				/* Looking for B
				 */
				$query = "select * from dh_values where contact_id=%d and serial_b=%d";
				if (($result = $this->execute($query, $contact_id, $serial_b)) == false) {
					return false;
				}
				$value_b = $result[0]["value_b"];

				/* Store new record
				 */
				$values = array(
					"id"         => null,
					"contact_id" => $contact_id,
					"serial_a"   => $serial_a,
					"serial_b"   => $serial_b,
					"value_a"    => $value_a,
					"value_b"    => $value_b,
					"crypto_key" => null,
					"confirmed"  => 1);

				if ($this->insert("dh_values", $values) === false) {
					return false;
				}

				$values["id"] = $this->last_insert_id;
			} else {
				$values = $result[0];
			}

			if ($values["crypto_key"] == null) {
				#printf("CRYPTO: Diffie-Hellman: calculating secret key for %d and %d...\n", $values["value_a"], $values["value_b"]);
				$dh = new DH($this->dh_prime, $this->dh_generator, $values["value_a"]);
				$values["crypto_key"] = $dh->calculate_key($values["value_b"]);

				$this->update("dh_values", $values["id"], array("crypto_key" => $values["crypto_key"]));
			}

			#printf("CRYPTO: %d %d -> %s\n", $values["serial_a"], $values["serial_b"], $values["crypto_key"]);

			return $values["crypto_key"];
		}

		/* Handle confirmed and new DH value
		 *
		 * INPUT:  integer contact id, integer 
		 * OUTPUT: true
		 * ERROR:  false
		 */
		public function handle_received_dh_stuff($contact_id, $serial_b, $serial_a, $new_serial_b, $new_value_b) {
			/* Remove deprecated confirmed values
			 */
			$query = "delete from dh_values where contact_id=%d and (serial_a<%d or serial_b<%d)";
			$this->execute($query, $contact_id, $serial_a, $serial_b);

			/* Confirm A
			 */
			$query = "update dh_values set confirmed=%d where contact_id=%d and serial_a=%d";
			if ($this->execute($query, 1, $contact_id, $serial_a) === false) {
				return false;
			}

			/* Store new B
			 */
			$query = "select count(*) as count from dh_values where contact_id=%d and serial_b=%d";
			if (($result = $this->execute($query, $contact_id, $new_serial_b)) === false) {
				return false;
			}

			if ($result[0]["count"] > 0) {
				return true;
			}

			$query = "select serial_a, value_a from dh_values where contact_id=%d and confirmed=%d ".
			         "order by serial_a desc limit 1";
			if (($result = $this->execute($query, $contact_id, 1)) == false) {
				return false;
			}
			$my_serial_a = $result[0]["serial_a"];
			$my_value_a = $result[0]["value_a"];

			$data = array(
				"id"         => null,
				"contact_id" => $contact_id,
				"serial_a"   => $my_serial_a,
				"serial_b"   => $new_serial_b,
				"value_a"    => $my_value_a,
				"value_b"    => $new_value_b,
				"crypto_key" => null,
				"confirmed"  => 1);

			return $this->insert("dh_values", $data) !== false;
		}

		/* Get Diffie-Hellman key information
		 *
		 * INPUT:  -
		 * OUTPUT: array contact info with DH values
		 * ERROR:  false
		 */
		public function get_dh_keys() {
			$query = "select c.name, c.address, v.serial_a, v.serial_b, v.confirmed ".
			         "from contacts c, dh_values v where c.id=v.contact_id ".
			         "order by c.id, v.serial_a, v.serial_b";

			return $this->execute($query);
		}
	}
?>
