<?php
	/* libraries/system.php
	 *
	 * Copyright (C) by Hugo Leisink <hugo@leisink.net>
	 */

	require("libraries/crypto/random.php");
	require("libraries/crypto/bigint.php");
	require("libraries/crypto/hash.php");
	require("libraries/crypto/rsa.php");

	require("libraries/crypto/base.php");
	require("libraries/crypto/rijndael.php");
	require("libraries/crypto/aes.php");

	define("PROTOCOL_VERSION", 1);
	define("FORMAT_VERSION", 1);

	define("STORAGE_DB", "storage.db");
/*
	define("DH_PRIME", "71611195866368241734230315014260885890178941731009".
                       "36846965880370246372095663312093529483110175757499".
                       "61619319828641955426693304570465688762892415366806".
                       "83601749507786059442920003278263334056542642264651");
*/
	define("DH_PRIME", "64495327731887693539738558691066839103388567300449");
	define("DH_GENERATOR", "2");

	define("RSA_KEY_SIZE", 2048);
	define("RSA_IV_SIZE", 32);
	define("ALIAS_SIZE", 30);
	define("DEVICE_IDENTIFIER_SIZE", 20);

	/* Auto-load class files
	 */
	function __autoload($class_name) {
		$class_name = strtolower($class_name);
		$paths = array("", "crypto/", "storage/");

		foreach ($paths as $path) {
			if (file_exists($file = "libraries/".$path.$class_name.".php")) {
				include_once($file);
				break;
			}
		}
	}

	/* Generate random string
	 */
	function random_string($length = 10) {
		$characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890";
		$max_pos = strlen($characters) - 1;

		$result = "";
		while ($length-- > 0) {
			$result .= $characters[mt_rand(0, $max_pos)];
		}

		return $result;
	}

	/* Read single line from user
	 */
	function read_line($info) {
		do {
			print "- ".$info.": ";
			$input = trim(fgets(STDIN));
		} while ($input == "");

		return $input;
	}

	/* Read multiple lines from user
	 */
	function read_text($info) {
		print "- ".$info.":\n";
		$input = "";
		while (($line = fgets(STDIN)) !== false) {
			$line = trim($line);
			if (trim($line) == ".") {
				break;
			}
			$input .= $line."\n";
		}

		return $input;
	}

	/* Convert binary to human readble text
	 */
	function bin_to_hex($binary) {
		$parts = str_split($binary, 16);
		$result = "";

		foreach ($parts as $part) {
			$result .= "\t";

			$len = strlen($part);

			for ($i = 0; $i < $len; $i++) {
				$result .= sprintf(" %02X", ord(substr($part, $i, 1)));
				if ($i == 7) {
					$result .= " ";
				}
			}

			if ($len < 16) {
				$result .= str_repeat("   ", 16 - $i);
				if ($len < 8) {
					$result .= " ";
				}
			}

			$result .= "  ";

			for ($i = 0; $i < $len; $i++) {
				$char = substr($part, $i, 1);
				if ((ord($char) >= 32) && (ord($char) < 127)) {
					$result .= $char;
				} else {
					$result .= ".";
				}
			}

			$result .= "\n";
		}

		return $result;
	}

	/* Global settings
	 */
	error_reporting(E_ALL & ~E_NOTICE);
?>
