<?php
	/* libraries/dh.php
	 *
	 * Copyright (C) by Hugo Leisink <hugo@leisink.net>
	 */

	class DH {
		private $prime = null;
		private $generator = null;
		private $private_value = null;
		private $public_value = null;

		/* Constructor
		 *
		 * INPUT:  string prime, string generator, integer maximum private key value
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function __construct($prime, $generator, $private_value = null) {
			$this->prime = $prime;
			$this->generator = $generator;

			if ($private_value === null) {
				$this->private_value = $this->random_bigint(620);
			} else {
				$this->private_value = $private_value;
			}

			$this->public_value = $this->bigint_pow_mod($this->generator, $this->private_value, $this->prime);
		}

		/* Magic method get
		 *
		 * INPUT:  string key
		 * OUTPUT: mixed value
		 * ERROR:  null
		 */
		public function __get($key) {
			switch ($key) {
				case "generator": return $this->generator;
				case "prime": return $this->prime;
				case "public_value": return $this->public_value->value;
				case "private_value": return $this->private_value;
			}

			return null;
		}

		/* Generate random big integer
		 *
		 * INPUT:  int length
		 * OUTPUT: string random big integer
		 * ERROR:  -
		 */
		private function random_bigint($length) {
			$characters = "01234567890";
			$max_pos = strlen($characters) - 1;

			$result = "";
			while ($length-- > 0) {
				$result .= $characters[mt_rand(0, $max_pos)];
			}

			return $result;
		}

		/* Exponentiate big integers modulus n
		 *
		 * INPUT:  string a, string b
		 * OUTPUT: string (a ^ b) mod n
		 * ERROR:  -
		 */
		private function bigint_pow_mod($a, $b, $n) {
			$a = new Math_BigInteger($a);
			$b = new Math_BigInteger($b);
			$n = new Math_BigInteger($n);

			return $a->powMod($b, $n);
		}

		/* Calculate key
		 *
		 * INPUT:  string public key
		 * OUTPUT: string key
		 * ERROR:  false
		 */
		public function calculate_key($public_value) {
			$key = $this->bigint_pow_mod($public_value, $this->private_value, $this->prime);

			return substr($key, 0, 80);
		}
	}
?>
