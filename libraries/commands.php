<?php
	/* libraries/commands.php
	 *
	 * Copyright (C) by Hugo Leisink <hugo@leisink.net>
	 */

	class commands {
		private $hisser = null;
		private $storage = null;
		private $dh_prime = null;
		private $dh_generator = null;

		/* Constructor
		 */
		public function __construct($hisser, $storage, $prime, $generator) {
			$this->hisser = $hisser;
			$this->storage = $storage;
			$this->dh_prime = $prime;
			$this->dh_generator = $generator;
		}

		/* Add hostname to Hisser address if it's missing
		 */
		private function fix_address(&$address) {
			if (strpos($address, "@") === false) {
				$address = sprintf("%s@%s", $address, $this->storage->get_setting("hostname"));
			}
		}

		/* List contacts
		 */
		public function show_contacts() {
			if (($contacts = $this->storage->get_contacts()) === false) {
				printf("Error reading contacts from storage.\n");
				return false;
			}

			printf("Validated contacts:\n");
			foreach ($contacts as $contact) {
				if (($contact["name"] != "") && ($contact["validated"] == 1)) {
					printf("- %s <%s>\n", $contact["name"], $contact["address"]);
				}
			}

			printf("\nNon-validated contacts:\n");
			foreach ($contacts as $contact) {
				if (($contact["name"] != "") && ($contact["validated"] == 0)) {
					$hash = hash("sha256", $contact["public_key"]);
					printf("- %s <%s>: %s\n", $contact["name"], $contact["address"], $hash);
				}
			}

			printf("\nPending invitations:\n");
			foreach ($contacts as $contact) {
				if ($contact["name"] == "") {
					printf("- %s\n", $contact["address"]);
				}
			}

			return true;
		}

		/* Delete message
		 */
		public function delete_message($message_id) {
			list($begin, $end) = explode("-", $message_id, 2);
			if ($end == null) {
				$message_ids = array($message_id);
			} else {
				$message_ids = range($begin, $end);
			}

			foreach ($message_ids as $message_id) {
				if ($this->storage->delete_message($message_id) == false) {
					printf("Error while deleting message.\n");
					return false;
				}
			}

			printf("Messages have been deleted.\n");
			return true;
		}

		/* Fetch messages from server
		 */
		public function fetch_messages() {
			if (($index = $this->hisser->get_index()) === false) {
				printf("Error reading index from server.\n");
				return false;
			}

			foreach ($index as $item) {
				if (($message = $this->hisser->get_message($item["id"])) === false) {
					printf("Error fetching message.\n");
					continue;
				}

				if ($item["type"] == "invitation") {
					/* Invitation
					 */
					$binary = new binary("s1s1s1s1s2s1s2n4");
					$binary->set_labels("receiver", "sender", "name", "alias", "public_key", "dh_value", "signature", "group_id");
					if (($message = $binary->decode($message)) === false) {
						printf("Error decoding invitation.\n");
						continue;
					}
					unset($binary);

					$handle_invitation = false;

					if (($contact = $this->storage->get_contact_by_address($message["sender"])) == false) {
						/* Invitation from new user
						 */
						$answer = read_line("Accept invitation from ".$message["name"]." <".$message["sender"].">");
						if ($answer == "yes") {
							if ($this->invite_user($message["sender"])) {
								if (($contact = $this->storage->get_contact_by_address($message["sender"])) != false) {
									$handle_invitation = true;
								}
							} else {
								printf("Error accepting invitation.\n");
								continue;
							}
						} else if ($answer == "no") {
							$this->hisser->delete_message($item["id"]);
						}
					} else if (($contact["name"] == null) && ($contact["alias"] == null)) {
						/* Previously sent invitation accepted
						 */
						printf("%s <%s> has accepted your invitation.\n", $message["name"], $message["sender"]);
						$handle_invitation = true;
					} else {
						/* Invitation from user already in contact list
						 */
						printf("Removing duplicate invitation from %s <%s> .\n", $message["name"], $message["sender"]);
						$this->hisser->delete_message($item["id"]);
					}

					if ($handle_invitation) {
						/* Handle invitation
						 */
						$signature = hash("sha256", $message["sender"].$message["name"].$message["alias"].$message["dh_value"]);

						$rsa = new Crypt_RSA();
						if ($rsa->loadKey($message["public_key"]) == false) {
							printf("Error loading RSA public key.\n");
							continue;
						} else if (($message["signature"] = $rsa->decrypt($message["signature"])) === false) {
							printf("Error decrypting signature.\n");
							$this->hisser->delete_message($item["id"]);
							continue;
						} else if ($message["signature"] !== $signature) {
							printf("Bad invitation signature.\n");
							$this->hisser->delete_message($item["id"]);
							continue;
						}

						$this->storage->complete_contact($contact["id"], $message["name"], $message["alias"], $message["public_key"], $message["dh_value"]);

						$this->hisser->delete_message($item["id"]);
					}
				} else if ($item["type"] == "message") {
					/* Message
					 */
					$binary = new binary("s1s4s2");
					$binary->set_labels("receiver", "message", "signature");
					if (($message = $binary->decode($message)) === false) {
						printf("Error decoding message.\n");
						continue;
					}
					unset($binary);

					$hash = hash("sha256", $message["message"]);
					$signature = $message["signature"];

					$binary = new binary("n1s1n4n4s1s4");
					if ($binary->set_labels("version", "receiver", "serial_a", "serial_b", "iv", "data") == false) {
						continue;
					}

					if (($message = $binary->decode($message["message"])) === false) {
						printf("Error decoding binary message.\n");
						continue;
					}

					if ($message["version"] != PROTOCOL_VERSION) {
						printf("Message sent via unsupported protocol version.\n");
						continue;
					}

					if (($contact = $this->storage->get_contact_by_my_alias($message["receiver"])) == false) {
						printf("Message received from unknown user.\n");
						$this->hisser->delete_message($item["id"]);
						continue;
					}

					/* Verify signature
					 */
					$rsa = new Crypt_RSA();
					if ($rsa->loadKey($contact["public_key"]) == false) {
						printf("Error loading RSA public key.\n");
						$this->hisser->delete_message($item["id"]);
						continue;
					} else if (($signature = $rsa->decrypt($signature)) === false) {
						printf("Error decrypting signature.\n");
						$this->hisser->delete_message($item["id"]);
						continue;
					}

					if ($hash !== $signature) {
						printf("Invalid signature!\n");
						continue;
					}

					/* Decrypt message
					 */
					$serial_a = $message["serial_a"];
					$serial_b = $message["serial_b"];

					if (($crypto_key = $this->storage->get_key_for_decryption($contact["id"], $serial_a, $serial_b)) == false) {
						printf("Error getting crypto values for a=%d and b=%d.\n", $serial_a, $serial_b);
						$this->hisser->delete_message($item["id"]);
						continue;
					}

					$aes = new Crypt_AES(CRYPT_AES_MODE_CBC);
					$aes->setKey($crypto_key);
					$aes->setIV($message["iv"]);

					$binary = new binary("n1s1n4n4s1s4");
					if ($binary->set_labels("version", "dh_value", "dh_serial", "group_id", "mimetype", "content") == false) {
						continue;
					}

					if (($message = $aes->decrypt($message["data"])) === false) {
						printf("AES decrypting error.\n");
						continue;
					}
					if (($message = $binary->decode($message)) === false) {
						printf("Error decoding binary data.\n");
						continue;
					}

					if (($message === false) || ($message === null)) {
						$this->hisser->delete_message($item["id"]);
						continue;
					}

					if ($message["version"] != FORMAT_VERSION) {
						printf("Message sent via unsupported format version.\n");
						continue;
					}

					$this->storage->handle_received_dh_stuff($contact["id"], $serial_a, $serial_b, $message["dh_serial"], $message["dh_value"]);

					/* Processing message
					 */
					switch ($message["mimetype"]) {
						case "text/plain":
							printf("- Message received from %s <%s>:\n%s\n", $contact["name"], $contact["address"], $message["content"]);
							$this->storage->save_message($contact["id"], $message["content"]);
							break;
						default:
							printf("Message of unsupported type (%s) received.\n", $message["mimetype"]);
					}

					$this->hisser->delete_message($item["id"]);
				} else {
					/* Unknown type
					 */
					printf("Message of unknown type received.\n");
					$this->hisser->delete_message($item["id"]);
				}
			}

			return true;
		}

		/* Show new messages on server
		 */
		public function show_inbox() {
			if (($index = $this->hisser->get_index()) === false) {
				printf("Error reading index from server.\n");
				return false;
			}

			printf("Id       Size       Type\n");
			printf("-------------------------------\n");
			foreach ($index as $item) {
				printf(" %-7d %-10d %-8s\n", $item["id"], $item["size"], $item["type"]);
			}

			return true;
		}

		/* Get message index
		 */
		public function show_message_index() {
			if (($messages = $this->storage->get_message_index()) === false) {
				printf("Error retreiving message index.\n");
				return false;
			}

			foreach ($messages as $message) {
				$content = substr($message["content"], 0, 50);
				$content = str_replace("\n", " ", $content);
				printf("%d: %s <%s>\n  %s\n", $message["id"], $message["name"], $message["address"], $content);
			}

			return true;
		}

		/* Invite other user on contact list
		 */
		public function invite_user($address, $group_id = null) {
			$this->fix_address($address);

			list($username, $servername) = explode("@", $address, 2);
			if ($servername == "") {
				printf("Invalid Hisser address.\n");
				return false;
			}

			$my_address = $this->storage->get_setting("username")."@".$this->storage->get_setting("hostname");
			if ($address == $my_address) {
				printf("You can't invite yourself.\n");
				return false;
			}

			if ($this->storage->get_contact_by_address($address) != false) {
				printf("That user is already on your contact list.\n");
				return false;
			}

			$alias = random_string(ALIAS_SIZE);

			if ($this->hisser->create_alias($alias) == false) {
				printf("Error creating alias.\n");
				return false;
			}

			$sender = $this->storage->get_setting("username")."@".$this->storage->get_setting("hostname");
			$name = $this->storage->get_setting("fullname");

			/* RSA public key
			 */
			$rsa = new Crypt_RSA();
			$rsa->loadKey($this->storage->get_setting("crypto_key"));
			$public_key = $rsa->getPublicKey();

			/* DH value
			 */
			$dh = new DH($this->dh_prime, $this->dh_generator);
			$dh_value = $dh->public_value;

			/* Signature
			 */
			$signature = hash("sha256", $sender.$name.$alias.$dh_value);

			$rsa = new Crypt_RSA();
			if ($rsa->loadKey($this->storage->get_setting("crypto_key")) == false) {
				printf("Error loading RSA private key.\n");
				return false;
			} else if (($signature = $rsa->encrypt($signature)) === false) {
				printf("Error encrypting DH value.\n");
				return false;
			}

			if ($this->storage->create_contact($address, $alias, $dh->private_value) == false) {
				$this->hisser->delete_alias($alias);
				return false;
			}

			$remote = new hisser(null, $servername);
			if ($remote->send_invitation($username, $sender, $name, $alias, $public_key, $dh_value, $signature) == false) {
				$this->hisser->delete_alias($alias);
				$this->storage->remove_contact($address);
				printf("Error sending invitation.\n");
				return false;
			}

			printf("Invitation sent.\n");
			return true;
		}

		/* Show my RSA hash
		 */
		public function show_my_hash() {
			$rsa = new Crypt_RSA();
			$rsa->loadKey($this->storage->get_setting("crypto_key"));
			$hash = hash("sha256", $rsa->getPublicKey());

			printf("Hash: %s\n", $hash);
		}

		/* Read message
		 */
		public function read_message($message_id) {
			if (($message = $this->storage->get_message($message_id)) == false) {
				printf("Message not found.\n");
				return false;
			}

			printf("- %s <%s>\n%s\n", $message["name"], $message["address"], trim($message["content"]));
			return true;
		}

		/* Send message
		 */
		public function send_message($address) {
			$this->fix_address($address);

			if (($contact = $this->storage->get_contact_by_address($address)) == false) {
				printf("User not found.\n");
				return false;
			}

			if ($contact["alias"] == "") {
				printf("User has not accepted your invitiation yet.\n");
				return false;
			} else if ($contact["validated"] == 0) {
				printf("User has not been validated yet.\n");
				return false;
			}

			$message = read_text("Enter your message");

			if (($crypto = $this->storage->get_crypto_values($contact["id"])) == false) {
				printf("Error getting crypto key.\n");
				return false;
			}
			$iv = random_string(RSA_IV_SIZE);

			$aes = new Crypt_AES(CRYPT_AES_MODE_CBC);
			$aes->setKey($crypto["crypto_key"]);
			$aes->setIV($iv);

			if (($dh = $this->storage->get_my_latest_dh_value($contact["id"])) == false) {
				printf("Error getting latest DH value.\n");
				return false;
			}

			/* Encrypt message
			 */
			$group_id = 0;
			$binary = new binary("n1s1n4n4s1s4");

			if ($binary->add_element(FORMAT_VERSION, $dh["dh_value"], $dh["dh_serial"], $group_id, "text/plain", $message) == false) {
				printf("Error adding message to binary class.\n");
				return false;
			} else if (($data = $binary->encode()) === false) {
				printf("Error encoding message to binary.\n");
				return false;
			} else if (($data = $aes->encrypt($data)) === false) {
				printf("AES encrypting error.\n");
				return false;
			}

			list(, $servername) = explode("@", $address, 2);

			$remote = new hisser(null, $servername);
			$private_key = $this->storage->get_setting("crypto_key");
			if ($remote->send_message($contact["alias"], $crypto["serial_a"], $crypto["serial_b"], $iv, $data, $private_key) == false) {
				printf("Error sending message.\n");
				return false;
			}

			printf("Message sent.\n");
			return true;
		}

		/* Remote user from contact list
		 */
		public function uninvite_user($address) {
			$this->fix_address($address);

			if (($contact = $this->storage->get_contact_by_address($address)) == false) {
				printf("User not in contact list.\n");
				return false;
			}

			if ($this->storage->remove_contact($address) == false) {
				printf("Error removing user from contact list.\n");
				return false;
			}

			$this->hisser->delete_alias($contact["alias"]);

			printf("User has been removed from the contact list.\n");
			return true;
		}

		/* Validate user
		 */
		public function validate_user($address) {
			$this->fix_address($address);

			if (($contact = $this->storage->get_contact_by_address($address)) == false) {
				printf("No user with that address is in your contact list.\n");
				return false;
			} else if ($contact["validated"] == 1) {
				printf("User already validated.\n");
				return false;
			} else if ($this->storage->validate_contact($contact["id"]) == false) {
				printf("Error while validating user.\n");
				return false;
			}

			printf("User validated.\n");
			return true;
		}

		/* Show Diffie-Hellman key information
		 */
		public function show_dh_keys() {
			if (($keys = $this->storage->get_dh_keys()) === false) {
				printf("Error while reading DH values.\n");
				return false;
			}

			foreach ($keys as $key) {
				printf("%s (%s): %d %d %s\n", $key["name"], $key["address"], $key["serial_a"], $key["serial_b"], $key["confirmed"] ? "yes" : "no");
			}
		}
	}
?>
