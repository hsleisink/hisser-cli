<?php
	/* libraries/https.php
	 *
	 * Copyright (C) by Hugo Leisink <hugo@leisink.net>
	 * This file is part of the Banshee PHP framework
	 * http://www.banshee-php.org/
	 */

	class HTTPS extends HTTP {
		protected $default_port = 443;
		protected $protocol = "tls";
		protected $cert_validate_callback = null;

		/* Set certificate validation callback
		 *
		 * INPUT:  function certificate validation callback
		 * OUTPUT: -
		 * ERROR:  -
		 */
		public function cert_validation_callback($callback) {
			$this->cert_validate_callback = $callback;
		}

		/* Connect to server
		 *
		 * INPUT:  -
		 * OUTPUT: resource socket
		 * ERROR:  false connection failed
		 */
		protected function connect_to_server() {
			$remote = sprintf("%s://%s:%s", $this->protocol, $this->connect_host, $this->connect_port);

			$context = stream_context_create(array("ssl" => array("capture_peer_cert" => true)));
			if (($sock = stream_socket_client($remote, $errno, $errstr, $this->timeout, STREAM_CLIENT_CONNECT, $context)) == false) {
				return false;
			}

			if ($this->cert_validate_callback !== null) {
				$params = stream_context_get_params($context);
				$peer_cert = openssl_x509_parse($params["options"]["ssl"]["peer_certificate"]);
				if (call_user_func($this->cert_validate_callback, $peer_cert) == false) {
					fclose($sock);
					return false;
				}
			}

			return $sock;
		}
	}
?>
