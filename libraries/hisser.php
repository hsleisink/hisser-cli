<?php
	/* libraries/hisser.php
	 *
	 * Copyright (C) by Hugo Leisink <hugo@leisink.net>
	 */

	class hisser extends HTTP {
		private $device_identifier = null;

		/* Constructor
		 */
		public function __construct($device_identifier, $host, $port = null) {
			$this->device_identifier = $device_identifier;

			parent::__construct($host, $port);
		}

		/* Destructor
		 */
		public function __destruct() {
			$this->GET("/logout");
		}

		/* Register device with Hisser server
		 */
		public function register($type, $token) {
			$data = array(
				"device"       => $this->device_identifier,
				"notify_type"  => $type,
				"notify_token" => $token);
			$result = $this->POST("/register", $data);

			return $result["status"] == 201;
		}

		/* Get all aliases for user
		 */
		public function get_aliases() {
			$result = $this->GET("/alias");
			if ($result["status"] != 200) {
				return false;
			}

			$binary = new binary("s1", 2);

			if (($aliases = $binary->decode($result["body"])) === false) {
				return false;
			}

			$result = array();
			foreach ($aliases as $alias) {
				array_push($result, $alias[0]);
			}

			return $result;
		}

		/* Create new alias for user
		 */
		public function create_alias($alias) {
			$result = $this->POST("/alias/create", array("alias" => $alias));

			return $result["status"] == 201;
		}

		/* Delete alias
		 */
		public function delete_alias($alias) {
			$result = $this->POST("/alias/delete", array("alias" => $alias));

			return $result["status"] == 200;
		}

		/* Send invitation to other user
		 */
		public function send_invitation($receiver, $sender, $name, $alias, $public_key, $dh_value, $signature, $group_id = 0) {
			$payload = array(
				"receiver"   => $receiver,
				"sender"     => $sender,
				"name"       => $name,
				"alias"      => $alias,
				"public_key" => $public_key,
				"dh_value"   => $dh_value,
				"signature"  => $signature,
				"group_id"   => $group_id);
			$result = $this->POST("/contactlist", $payload);

			return $result["status"] == 201;
		}

		/* Get message index at server
		 */
		public function get_index() {
			$result = $this->GET("/message/index?device=".$this->device_identifier);
			if ($result["status"] == 204) {
				return array();
			} else if ($result["status"] != 200) {
				return false;
			}

			$binary = new binary("n4n4s1", 2);
			if ($binary->set_labels("id", "size", "type") == false) {
				return false;
			}

			return $binary->decode($result["body"]);
		}

		/* Get single message from server
		 */
		public function get_message($message_id) {
			$path = sprintf("/message?id=%d", $message_id);
			$result = $this->GET($path);

			if ($result["status"] != 200) {
				return false;
			}

			return $result["body"];
		}

		/* Delete single message from server
		 */
		public function delete_message($message_id) {
			$result = $this->POST("/message/delete", "id=".$message_id);

			return $result["status"] == 200;
		}

		/* Send message to other user
		 */
		public function send_message($receiver, $serial_a, $serial_b, $iv, $data, $private_key) {
			$binary = new binary("n1s1n4n4s1s4");
			$binary->add_element(PROTOCOL_VERSION, $receiver, $serial_a, $serial_b, $iv, $data);
			if (($message = $binary->encode()) === false) {
				printf("Error encoding binary message.\n");
				return false;
			}

			$hash = hash("sha256", $message);

			$rsa = new Crypt_RSA();
			if ($rsa->loadKey($private_key) == false) {
				printf("Error loading RSA private key.\n");
				return false;
			} else if (($signature = $rsa->encrypt($hash)) === false) {
				printf("Error encrypting message hash.\n");
				return false;
			}

			$payload = array(
				"receiver"  => $receiver,
				"message"   => $message,
				"signature" => $signature);

			$result = $this->POST("/message", $payload);

			return $result["status"] == 201;
		}
	}
?>
